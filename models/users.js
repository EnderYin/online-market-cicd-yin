let mongoose = require("mongoose")

let UserSchema = new mongoose.Schema(
    {
        username: String,
        password: {type: Number, default: 0}
    },
    { collection: "users" })

module.exports = mongoose.model("user", UserSchema)
