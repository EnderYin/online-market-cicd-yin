let mongoose = require("mongoose")

let InvoiceSchema = new mongoose.Schema(
  {
    name: String,
    buyer: String,
    seller: String,
    message: {type: String, default: ""}
  },
  { collection: "invoices" })

module.exports = mongoose.model("invoice", InvoiceSchema)



/*var invoices = [
    {id : 1000000, seller: 'jobs', buyer: 'jack', name: 'iphone8'},

];

module.exports = invoices;*/
