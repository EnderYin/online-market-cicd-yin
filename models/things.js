let mongoose = require("mongoose")

let ThingSchema = new mongoose.Schema(
  {
    name: String,
    seller: String,
    price: {type: Number, default: 0}
  },
  { collection: "things" })

module.exports = mongoose.model("thing", ThingSchema)


/*var things = [
    {name: "iphone8", seller: 'jobs', price: 1000},
    {name: "macbook", seller: 'tom',  price: 1500}
];

module.exports = things; */
