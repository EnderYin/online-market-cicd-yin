const chai = require("chai")
const expect = chai.expect
let server = require("../bin/www")
const request = require("supertest")
const _ = require("lodash")
const { MongoClient } = require("mongodb")
const dotenv = require("dotenv")
dotenv.config()

let datastore , datastore1

let db, client,  validID

describe("Things",  (done) => {
  before(async () => {
    try {
      client = await MongoClient.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      })
      db = client.db(process.env.MONGO_DB)
      //server = require("../bin/www")
      datastore = db.collection("things")
      datastore1 = db.collection("invoices")

      //server = require("../../../bin/www");
    } catch (error) {
      console.log(error)
    }
    //done()
  })

  beforeEach(async () => {
    try {
      await datastore.deleteMany({})
      await datastore.insertOne({name: "iphone8", seller: "jobs", price: 1000})
      await datastore.insertOne({name: "macbook", seller: "tom", price: 1500})

      await datastore1.deleteMany({})
      await datastore1.insertOne({id: 1000000, seller: "jobs", buyer: "jack", name: "iphone8"})

      const donation = await datastore.findOne({ name: "iphone8" })
      validID = donation._id
    }  catch (error) {
      console.log(error)
    }
    //datastore1.push({id : 1000001, seller: 'tom',  buyer: 'jerry', name: 'macbook'});
  })

  describe("GET /things", () => {
    it("hhh should return all the things", done => {
      request(server)
        .get("/things")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          //expect(res.body).to.be.a("array")
          expect(res.body.length).to.deep.equal(2)
          const result = _.map(res.body, thing => {
            return { name: thing.name, seller: thing.seller }
          })
          expect(result).to.deep.include({name: "iphone8", seller: "jobs"})
          expect(result).to.deep.include({name: "macbook", seller: "tom" })

          done(err)
        })
    })
  })

  describe("GET /things/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching thing", () => {
        request(server)
          .get(`/things/${validID}`)
          .expect(200)
          .end((err, res) => {
            const data = res.body
            expect(data[0]).to.deep.include({name: "iphone8", seller: "jobs", price: 1000})
            //done(err)
          })
      })
    })
    describe("when the id is invalid", () => {
      it("should return the NOT found message", done => {
        request(server)
          .get("/things/none")
          .set("Accept", "application/json")
          .expect(200)
          .expect({ message: "thing NOT Found!" }, (err) => {
            done(err)
          })
      })
    })
  })

  describe("POST /things", () => {
    it("should return confirmation message and update datastore", () => {
      const thing = {name: "iphone9", seller: "jobs", price: 1100}
      return request(server)
        .post("/things")
        .send(thing)
        .expect({ message: "thing Successfully put!" })
    })
    after(() => {
      return request(server)
        .get("/things")
        .expect(200)
        .then(res => {
          expect(res.body.length).equals(3)
          const result = _.map(res.body, thing => {
            return {
              name: thing.name,
              seller: thing.seller
            }
          })
          expect(result).to.deep.include({ name: "iphone9", seller: "jobs"})
        })
    })
  })

  describe("PUT /things/:id", () =>{
    //this.timeout(500)
    it("should return a confirm message and update the datastore's price", () => {
      const price = {price : 1001}
      return request(server)
        .put(`/things/${validID}`)
        .send(price)
        .expect(200)
        .then(res => {
          expect(res.body).to.include({ message: "price change Successfully!"})
          //setTimeout(done, 500);
          //done()
          //this.timeout(500);
        })
      //this.timeout(500);
    })
    after(() => {
      return request(server)
          .get(`/things/${validID}`)
          .expect(200)
          .then(res => {
            expect(res.body[0]).to.deep.include({name: "iphone8", seller: "jobs", price: 1001})
            //expect(res.body[0]).to.have.property("paymenttype", "Visa");
            //process.exit()
          });


    });
  })


  describe("DELETE /things/:id", () =>{
    describe("if id is valid" , () => {
    it(" should return delete successfully", () => {
      return request(server)
        .delete(`/things/${validID}`)

        .expect(200)
        .then(res => {
          expect(res.body).to.include({ message: "Thing Successfully Deleted!"})

        })
    })
    after(() => {
      return request(server)
        .get("/things")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .then(res => {
          expect(res.body).not.to.include({name: "iphone8", seller: "jobs", price: 1000})
        })
    })
    })

    describe("if id is invalid" , () => {
      it(" should return not deleted", () => {
        return request(server)
            .delete(`/things/none`)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .then(res => {
              expect(res.body).to.include({ message: "Thing NOT DELETED!"})

            })
      })
      after(() => {
        return request(server)
            .get("/things")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then(res => {
              expect(res.body).not.include({name: "iphone8", seller: "jobs", price: 1000})
            })
      })
    })

  })
  /*
  describe("GET /invoices/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching donation", done => {
        request(server)
          .get(`/invoices/${datastore1[0].id}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) => {
            expect(res.body).to.deep.include(datastore1[0])
            done(err)
          })
      })
    })
    describe("when the id is invalid", () => {
      it("should return the NOT found message", done => {
        request(server)
          .get("/invoices/9999")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .expect({ message: "invoice NOT Found!" }, (err) => {
            done(err)
          })
      })
    })
  })

  describe("POST /invoices", () => {
    it("should return confirmation message and update datastore", () => {
      const buyer = { name : "macbook", buyer : "jerry"}
      return request(server)
        .post("/invoices")
        .send(buyer)
      //.expect({ message: "invoice Successfully created!" });
    })
    after(() => {
      return request(server)
        .get(`/invoices/${datastore1[0].id}`)
        .send()
        .then( () => {
          expect(datastore1[0])
        })
    })
  })

  describe("DELETE /inovices/:id", () =>{
    it("should return delete successfully", () => {
      return request(server)
        .delete(`/invoices/${datastore1[0].id}`)
        .expect(200)
        .then(res => {
          expect(res.body).to.include({ message: "invoice NOT DELETED!"})

        })
    })
    after(() => {
      return request(server)
        .get(`/invoices/${datastore1[0].id}`)
        .then(res => {
          expect(res.body).not.to.include({id : 1000000, seller: "jobs", buyer: "jack", name:"iphone8"})
        })
    })
  })
*/

  //setTimeout(done, 1000);

  describe("", ()=>{
    it( "" , () =>{
      process.exit()
    })
  })
});


