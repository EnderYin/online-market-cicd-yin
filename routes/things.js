let Thing = require("../models/things")
let Invoice = require("../models/invoices")
let User = require("../models/users")
let express = require("express")
let router = express.Router()

let mongoose = require("mongoose")
const dotenv = require("dotenv")
dotenv.config()
const uri = `${process.env.MONGO_URI}${process.env.MONGO_DB}`
console.log(uri)
mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

let db = mongoose.connection

db.on("error", function(err) {
  console.log("connection error", err)
})
db.once("open", function() {
  console.log("connected to database" + db.name)
})

/*let mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/onlinemarket');

let db = mongoose.connection;

db.on('error', function (err) {
    console.log('Unable to Connect to [ ' + db.name + ' ]', err);
});

db.once('open', function () {
    console.log('Successfully Connected to [ ' + db.name + ' ]');
});*/

let findByName = (arr, name) => {
  let result  = arr.filter(function(o) { return o.name === name} )
  return result ? result[0] : null // or undefined
}
let findById = (arr, id) => {
  let result  = arr.filter(function(o) { return o.id == id} )
  return result ? result[0] : null // or undefined
}


router.getauser = (req, res) => {
  /**/
  res.setHeader('Content-Type', 'application/json');
  //{ name : { $regex: req.params.name }
  User.find({username : req.params.username} ,function(err, user) {
    if (user.length === 0)
      res.send({ message: 'NOT Found!'} );
    else {
      res.send(user);
    }
  });
}

router.getusers = (req, res) =>{
  User.find( function (err, things) {
    if(err)
      res.send(err);

    res.send(things);
  } );
}

router.deleteuser = (req, res) => {
  User.findByIdAndRemove( {"_id" : req.params.id}, function (err) {
    if (err)
      res.json({ message: 'invoice NOT DELETED!', errmsg : err } );
    else
      res.json({ message: 'invoice Successfully Deleted!'});
  })
}

router.findAThingByUsername = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  //{ name : { $regex: req.params.name }
  Thing.find({"seller" : req.params.username} ,function(err, things) {
    if (things.length === 0)
      res.send([]);
    else {
      res.send(things)
    }
  });
}

router.findAInvoiceByUsername = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  //{ name : { $regex: req.params.name }
  Invoice.find({"buyer" : req.params.username} ,function(err, things) {
    if (things.length === 0)
      res.send([]);
    else {
      res.send(things)
    }
  });
}

router.login = (req, res ) => {
  res.setHeader('Content-Type', 'application/json');
  User.find( {"username" : req.body.username}, function(err, user){
    if(user.length === 0)
      res.send("user doesn't exist")
    else {
      if(user[0].password == req.body.password){ res.send("welcome") }
      else { res.send("password incorrect") }
    }
  })
}

router.register =(req, res) => {
  let username = req.body.username
  let password = req.body.password

  User.find ( {"username" : username}, function (err, user) {
    if(user.length === 0) {
      //res.send("you can use this name")
      let newuser = new User();

      newuser.username = username;// the requested value
      newuser.password = password;// the requested value

      newuser.save(function (err) {
        if (err)
          res.json({message: 'failed'});// return a suitable error message
        else
          res.send("user create successfully");// return a suitable success message
      });
    }else{
      res.send("user already exist")
    }
  })
}

router.findAll = (req, res) => {
  // Return a JSON representation of our list
  //res.setHeader('Content-Type', 'application/json');
  Thing.find( function (err, things) {
    if(err)
      res.send([]);

    res.send(things);
  } );
}

router.findAllInvoice = (req, res) => {
  Invoice.find( function (err, invoices) {
    if(err)
      res.send(err)
    else
    res.send(invoices)
  })
}

router.findAThing = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  //{ name : { $regex: req.params.name }
  Thing.find({"_id" : req.params.id} ,function(err, things) {
    if (err)
      res.json({} );
    else
      res.send(things);
  });
}

router.putathing = (req, res) => {
  res.setHeader('Content-Type', 'application/json');

  var thing = new Thing();

  thing.name = req.body.name;// the requested value
  thing.seller = req.body.seller;// the requested value
  thing.price = req.body.price;

  thing.save(function(err) {
    if (err)
      res.json({message:'failed', errmsg: err});// return a suitable error message
    else
      res.json({ message: 'thing Successfully put!'});// return a suitable success message
  });
}

router.changeprice = (req, res) => {
  Thing.findById({"_id" : req.params.id},  function(err,thing) {
    if (err)
      res.json({ message: ' NOT Found!', errmsg : err } );
    else
    {
      thing.price = req.body.price;
      thing.save(function (err) {
        if (err)
          res.json({ message: 'Fail!', errmsg : err } );
        else
          res.json({ message: 'price change Successfully!'});
      });
    }
  })
}

router.deletething = (req, res) => {
  //Delete the selected donation based on its id
  Thing.findByIdAndRemove({"_id" : req.params.id}, function(err) {
    if (err)
      res.json({ message: 'Thing NOT DELETED!'} );
    else
      res.json({ message: 'Thing Successfully Deleted!'});
  });
}

router.findaInvoice = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  Invoice.find( { "_id" : req.params.id },function(err, invoice) {
    if (err)
      res.json({ message: 'invoice NOT Found!', errmsg : err } );
    else
      res.send(JSON.stringify(invoice,null,5));
  });
}

router.buyAThing = (req, res) => {
  //const invoice = new Invoice();
  res.setHeader('Content-Type', 'application/json');
  Thing.find({ "_id" : req.body._id } ,function(err, things) {
    const invoice = new Invoice();
    invoice.name = things[0].name;
    invoice.buyer = req.body.buyer;
    invoice.seller = things[0].seller;
    invoice.message = "";
    invoice.save(function(err) {
      if (err)
        res.send('failed');// return a suitable error message
      else
        res.json('invoice Successfully created!');// return a suitable success message
    });
  });

}

router.addmessage = (req, res) => {
  Invoice.findById({ "_id" : req.params.id },  function(err,invoice) {
    if (err)
      res.json({ message: ' NOT Found!', errmsg : err } );
    else
    {
      invoice.message = req.body.message;
      invoice.save(function (err) {
        if (err)
          res.send('Fail!' );
        else
          res.send('add message Successfully!');
      });
    }
  })
}

router.deleteAinvoice = (req, res) => {
  Invoice.findByIdAndRemove({"_id" : req.params.id}, function(err) {
    if (err)
      res.json({ message: 'invoice NOT DELETED!', errmsg : err } );
    else
      res.json({ message: 'invoice Successfully Deleted!'});
  });
}

module.exports = router
